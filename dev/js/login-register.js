checkStateLogin = function() {
	if (document.cookie.indexOf("yaapshopping_remindme") != -1 || $("#logged-user").val() === "true") {
		$("#botones-usuario").show();
		$("#botones-acceso-reg").css("display","none");
			getUserID();
			push_bi_start('cd_login', 'loged');
		return true;
	} else {
		$("#botones-usuario").css("display","none");
		$("#botones-acceso-reg").show();
					push_bi_start('cd_login', 'not_loged');
		return false;
	}
};

checkLogin = function(){
	$(":checkbox").labelauty({
		label: false
	});
	var resultado = $("#logged-result").val();
	if(resultado === "true") {
		location.reload();
		$("#modalLogin").modal('hide');
		$("#botones-acceso-reg").css("display","none");
		$("#botones-usuario").css("display","block");
	} else if (resultado === "false") {
		$("#message-login").text("El usuario y/o contraseña es incorrecto.");
		$("#input_name").css("border-color", "red").css("box-shadow","0px 1px 1px rgba(45,225,169,0.075) inset,0px 0px 8px red");
		$("#input_Password").css("border-color", "red").css("box-shadow","0px 1px 1px rgba(45,225,169,0.075) inset,0px 0px 8px red");
	}
};