$(document).on("click touchstart", '.not-loggued-active-coupon', function(event) {
	event.stopPropagation();
	if ($(this).parents(".Buttons-categories").find('.discount-value').data('typeoffer') == 5) {
		$(".modal-login-reg-active-coupon-store-fragment").css('display', 'block');
		$("#modal-login-reg-active-coupon-store").modal('show');
	} else if ($(this).parents(".Buttons-categories").find('.discount-value').data('typeoffer') == 100) {
		$(".modal-login-reg-active-coupon-universal-fragment").css('display', 'block');
		$("#modal-login-reg-active-coupon-universal").modal('show');
	}
	return false;
});

$(document).on("click touchstart", '.active-coupon-of-store', function(event) {
	event.stopPropagation();
	// $(".modal-login-reg-active-coupon-store-fragment").css('display', 'block');
	// $("#modal-login-reg-active-coupon-store").modal('show');
	var couponId = "";
	couponId = $(this).parents(".Buttons-categories").find('.discount-value').data('identifier');
	if (typeof commandSetCoupon != 'undefined') {
				commandSetCoupon([{
					name: 'couponId',
					value: couponId
				}, {
					name: 'state',
					value: 'TRUE'
				}]);
			}
	return false;
});

$(document).on("click touchstart", '.coupon-active-of-store', function(event) {
	event.stopPropagation();
	// $(".modal-login-reg-active-coupon-store-fragment").css('display', 'block');
	// $("#modal-login-reg-active-coupon-store").modal('show');
	var couponId = "";
	couponId = $(this).parents(".Buttons-categories").find('.discount-value').data('identifier');
	if (typeof commandSetCoupon != 'undefined') {
				commandSetCoupon([{
					name: 'couponId',
					value: couponId
				}, {
					name: 'state',
					value: 'FALSE'
				}]);
			}
	return false;
});

$(document).on("click touchstart", '.active-card-of-store', function(event) {
	event.stopPropagation();
	$(".modal-active-coupon-store-without-card-fragment").css('display', 'block');
	$("#modal-active-coupon-store-without-card").modal('show');
	return false;
});

$(document).on("click touchstart", '.active-coupon', function(event) {
	event.stopPropagation();
	$(".modal-active-coupon-universal-fragment").css('display', 'block');
	$("#modal-active-coupon-universal").modal('show');
	return false;
});

$(document).on("click touchstart", '.active-more-coupon-of-store', function(event) {
	event.stopPropagation();
	$(".modal-active-coupon-store-have-more-fragment").css('display', 'block');
	$("#modal-active-coupon-store-have-more").modal('show');
	return false;
});

