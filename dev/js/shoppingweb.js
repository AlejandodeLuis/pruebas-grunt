var map, marker, hasta, latLng, desde, directionsDisplay, directionsService, tienda, isMobile, $grid,
	centerStore = false, indiceScrollMark = 0, storesShow, defaultByOrder = false, promo = true,
	mapaViewed = false,	city = 0, locality = "Madrid", geocoder;

var getCookie = function(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
	}
	return "";
};

var getUserID = function() {
	var userId = getCookie("idUser");
	if (userId !== "") {
		push_bi_start("cd_user_id", userId);
	}
};

//Script para el Modal Descargar APP
var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
	any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

window.onload = function() {
	shoppingweb();
	checkStateLogin();
	allCookies();
	checkFollowing();
	resizeAddress();
	checkBalls();
	checkPromotions();
	checkStatePromo();
	prettyFollowers();
	$(":checkbox").labelauty({
		label: false
	});
};

window.onresize = function(event) {
	resizeAddress();
};

var checkPromo = function() {
	promo = $("#checkPromo").val();
	if (promo === "true") {
		return true;
	}
	return false;
};

var isotope = function() {
	$(".grid-item").eq(0).addClass("grid-sizer");

	// init Isotope
	$grid = $('.grid').isotope({
		itemSelector: '.grid-item',
		layoutMode: 'masonry',
		masonry: {
			columnWidth: '.grid-sizer'
		},
	});

	// layout Isotope after each image loads
	$grid.imagesLoaded().progress(function() {
		$grid.isotope('layout');
	});
};

var getParameterByName = function(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

var checkStoresShow = function() {
	if (parseInt($("#number-stores-hidden-input").val()) === $('.main-store-list').length) {
		$("#container-button-view-more").css("display", "none");
	} else {
		$("#container-button-view-more").css("display", "block");
	}
};

var defaultOrder = function() {
	defaultByOrder = true;
	$(".button-default").addClass("active-sort");
	$(".button-distance").removeClass("active-sort");
	checkStoresShow();
};

var prettyDistance = function() {
	if (defaultByOrder === false && desde !== undefined) {
		$('.container-distance-store').css('display', 'block');
		$(".store-distance").each(function(index, key) {
			if ($(this).text() !== "") {
				var distancia = parseFloat($(this).text());
				if (distancia < 1) {
					distancia = distancia * 1000;
					distancia = Math.round(distancia);
					$(this).text(distancia + " m");
				} else if (distancia >= 1 && distancia <= 50) {
					distancia = distancia.toFixed(2);
					$(this).text(distancia + " km");
				} else if (distancia > 50) {
					$(this).text(" +50 km");
				}
			}
		});
	} else {
		$('.container-distance-store').css('display', 'none');
	}
	prettyFollowers();
};

var prettyFollowers = function() {
	$(".seguidores").each(function() {
		var number = $(this).text();
		number = numberWithPoints(number);
		$(this).text(number);
	});

	function numberWithPoints(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}
};

var getStoresShow = function() {
	$('#overlay').show();
	storesShow = [];
	$($(".main-store-list").get().reverse()).each(function(index, el) {
		storesShow.push($(this).html());
	});
};

var putStores = function() {
	for (var i = 0; i < storesShow.length; i++) {
		$('<div class="col-xs-6 col-sm-6 col-md-4 col-md-offset-0 col-lg-4 main-store-list home grid-item" ">' + storesShow[i] + '</div>').insertBefore($(".main-store-list").eq(0));
	}
	$('html, body').animate({
		scrollTop: $(".main-store-list").eq(storesShow.length).offset().top - 10
	}, 1000, 'swing');
	$('#overlay').fadeOut();
	checkStoresShow();
};

var shoppingweb = function() {

	locationUser = function() {
		isotope();
		if (desde === undefined) {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(handle_geolocation_query, handleLocationError);
			} else {
				// Browser doesn't support Geolocation
				handleLocationError();
			}
		} else {
			$(".container-distance-store").css("display", "block");
		}
	};

	mobilecheck = function() {
		var check = false;
		(function(a) {
			if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
		})(navigator.userAgent || navigator.vendor || window.opera);
		return check;
	};

	modalInstructions = function() {
		if (!mobilecheck()) {
			$(".modal-instructions").show().removeClass("col-xs-12").addClass("col-xs-6").css("height", parseInt($("#map").css("height")) + "px");
			$("#map").removeClass("col-xs-12").addClass("col-xs-6");
			$("#linkOpenAppMap").css("display", "none");
		} else {
			$("#linkOpenAppMap").css("display", "block");
			$('.modal-instructions').css("display", "none");
			$("#map").removeClass("col-xs-6").addClass("col-xs-12");
			if (isMobile.Android()) {
				$("#linkOpenAppMap").attr("href", "http://www.google.com/maps/@" + hasta.lat + "," + hasta.lng);
				$("#logo-maps").attr("src", "../images/gmaps-icon.png");
			} else if (isMobile.iOS()) {
				$("#linkOpenAppMap").attr("href", "http://maps.apple.com/?ll=" + hasta.lat + "," + hasta.lng);
				$("#logo-maps").attr("src", "../images/maps_icon.png");
			}
		}
		centerMap();
	};

	eventListeners = function(tienda) {
		$(document).on("touchstart click", "#orderByPopularity", function() {
			checkLocality();
			if (typeof commandAllLocationsNotMap != 'undefined') {
				commandAllLocationsNotMap([{
					name: 'locality',
					value: localitySelected
				}, {
					name: 'latitude',
					value: 0.0
				}, {
					name: 'longitude',
					value: 0.0
				}]);
			}
			$(".button-default").addClass("active-sort");
			$(".button-distance").removeClass("active-sort");
			defaultByOrder = true;
			mapaViewed = false;
		});
		$(document).on("touchstart click", "#orderByDistance", function() {
			checkLocality();
			defaultByOrder = false;
			if (desde === undefined) {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(handle_geolocation_query, handleLocationErrorButton);
				} else {
					// Browser doesn't support Geolocation
					handleLocationError();
				}
			} else {
				command([{
					name: 'latitude',
					value: desde.lat()
				}, {
					name: 'longitude',
					value: desde.lng()
				}, {
					name: 'locality',
					value: localitySelected
				}]);
				$(".button-default").removeClass("active-sort");
				$(".button-distance").addClass("active-sort");
			}
		});

		$(document).on("touchstart click", ".filterBar", function(event) {
			city = 1;
			if (mapaViewed === true) {
				getAllStoresFromProvince();
			} else {
				commandOrderByDistanceStoresSelectedBar([{
					name: 'locality',
					value: 'Barcelona'
				}, {
					name: 'latitude',
					value: 0.0
				}, {
					name: 'longitude',
					value: 0.0
				}]);
			}

			if ($(".btn-Sel-City").length !== 0) {
				$(".btn-Sel-City").text('Barcelona ');
				$(".btn-Sel-City").append('<i class="fa fa-angle-down"></i></button>');
				$(".filterBar").children().remove();
				$(".filterBar").append("<li><span>Madrid</span></li>");
				$(".filterBar").addClass("filterMad");
				$(".filterBar").removeClass("filterBar");
				$(".btn-Sel-City").dropdown("toggle");
			} else {
				$(".filterBar").addClass('active-sort');
				$(".filterMad").removeClass('active-sort');
			}
		});

		$(document).on("touchstart click", ".filterMad", function(event) {
			city = 0;
			if (mapaViewed === true) {
				getAllStoresFromProvince();
			} else {
				commandOrderByDistanceStoresSelectedMad([{
					name: 'locality',
					value: 'Barcelona'
				}, {
					name: 'latitude',
					value: 0.0
				}, {
					name: 'longitude',
					value: 0.0
				}]);
			}

			if ($(".btn-Sel-City").length !== 0) {
				$(".btn-Sel-City").text('Madrid ');
				$(".btn-Sel-City").append('<i class="fa fa-angle-down"></i></button>');
				$(".filterMad").children().remove();
				$(".filterMad").append("<li><span>Barcelona</span></li>");
				$(".filterMad").addClass("filterBar");
				$(".filterMad").removeClass("filterMad");
				$(".btn-Sel-City").dropdown("toggle");
			} else {
				$(".filterBar").removeClass('active-sort');
				$(".filterMad").addClass('active-sort');
			}
		});

		$(document).on("touchstart click", "#action-ver-mapa", function(event) {
			mapaViewed = true;
			getAllStoresFromProvince();
		});

		$(document).on("touchstart click", "#action-ver-listado", function(event) {
			checkLocality();
			mapaViewed = false;
			if (desde !== undefined) {
				if (typeof command != 'undefined') {
					command([{
						name: 'locality',
						value: localitySelected
					}, {
						name: 'latitude',
						value: desde.lat()
					}, {
						name: 'longitude',
						value: desde.lng()
					}]);
				}
			} else {
				if (typeof commandAllLocationsNotMap != 'undefined') {
					commandAllLocationsNotMap([{
						name: 'locality',
						value: localitySelected
					}, {
						name: 'latitude',
						value: 0.0
					}, {
						name: 'longitude',
						value: 0.0
					}]);
				}
			}
		});

		$(document).on("touchstart click", "#container-distance-store-action", function(event) {
			tienda = {
				lat: parseFloat($(this).parents(".store-box").find(".latitudes").children().val()),
				lng: parseFloat($(this).parents(".store-box").find(".latitudes").children().next("input").val())
			};
			$("#mapaGoogle").modal('show');
			$('.modal-instructions').hide();
			initMap(tienda, "container-distance-store-action");
			return false;
		});

		$(document).on("touchstart click", "#foot, .fa-male", function() {
			$(".maps-btn i").removeClass("button-go-to-shop");
			$(".fa-male").addClass("button-go-to-shop");
			buttonTap("foot", directionsDisplay, directionsService);
			modalInstructions();
			return false;
		});

		$(document).on("touchstart click", "#car, .fa-car", function() {
			$(".maps-btn i").removeClass("button-go-to-shop");
			$(".fa-car").addClass("button-go-to-shop");
			buttonTap("drive", directionsDisplay, directionsService);
			modalInstructions();
			return false;
		});

		$(document).on("touchstart click", "#bus, .fa-bus", function() {
			$(".maps-btn i").removeClass("button-go-to-shop");
			$(".fa-bus").addClass("button-go-to-shop");
			buttonTap("transit", directionsDisplay, directionsService);
			modalInstructions();
			return false;
		});

		$(document).on("touchstart click", "#directionMap", function(event) {
			tienda = {
				lat: parseFloat($(this).find(".latitudes").children().val()),
				lng: parseFloat($(this).find(".latitudes").children().next("input").val())
			};
			centerStore = true;
			$("#mapaGoogle").modal('show');
			$('.modal-instructions').hide();
			initMap(tienda);
			return false;
		});

		$(document).on("touchstart click", "#location, .fa-location-arrow.boton-seguir", function() {
			centerStore = true;
			initMap(tienda);
			$('.modal-instructions').hide();
			return false;
		});

	};

	handle_geolocation_query = function(position) {
		function sendCommand(position) {
			$("#btn-dropdown-city").text(locality);
			if (typeof command != 'undefined') {
				command([{
					name: 'latitude',
					value: position.coords.latitude
				}, {
					name: 'longitude',
					value: position.coords.longitude
				}, {
					name: 'locality',
					value: locality
				}]);
			}
		}

		if (typeof google != 'undefined') {
			geocoder = new google.maps.Geocoder();
			desde = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			geocoder.geocode({
				'latLng': desde
			}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[1]) {
						getLocality(results);
					}
					sendCommand(position);
				} else {
					sendCommand(position);
				}
			});


		}
		$(".button-default").removeClass("active-sort");
		$(".button-distance").addClass("active-sort");
	};

	handleLocationError = function() {
		$("#car, #car i, #car p").parent().css("display", "none");
		$("#bus, #bus i, #bus p").parent().css("display", "none");
		$("#foot, #foot i, #foot p").parent().css("display", "none");
		isotope();
		$(".button-default").addClass("active-sort");
		$(".button-distance").removeClass("active-sort");
	};
	// END GEOLOCATION USER.

	handleLocationErrorButton = function() {
		$(".modal-user-not-have-geo-fragment").css("display", "block");
		$("#modal-order-by-distance-error").modal("show");
		$(".button-default").addClass("active-sort");
		$(".button-distance").removeClass("active-sort");
	};
	// END GEOLOCATION USER.

	getLocality = function(results) {
		var result = results[0];
		//look for locality tag and administrative_area_level_1
		var userCity = "";
		var state = "";
		for (var i = 0, len = result.address_components.length; i < len; i++) {
			var ac = result.address_components[i];
			if (ac.types.indexOf("locality") >= 0) userCity = ac.long_name;
			if (ac.types.indexOf("administrative_area_level_1") >= 0) state = ac.long_name;
		}
		locality = userCity;
		if (userCity != "Madrid" && userCity != "Barcelona") {
			locality = "Madrid";
		} else if (userCity === "Barcelona") {
			$(".btn-Sel-City").text("Barcelona");
			$(".filterBar").parent().html("<span class='filterMad'>Madrid</span>");
		}
	};

	initMap = function(tienda, exceptionHandler) {
		$(".maps-btn i").removeClass("button-go-to-shop");
		$(".fa-location-arrow").addClass("button-go-to-shop");
		$("#linkOpenAppMap").css("display", "none");
		$("#map").addClass("col-xs-12").removeClass("col-xs-6");
		directionsDisplay = new google.maps.DirectionsRenderer({
			suppressMarkers: true
		});
		directionsService = new google.maps.DirectionsService({
			suppressMarkers: true
		});
		hasta = tienda;

		// CREATE MAP
		map = new google.maps.Map(document.getElementById('map'), {
			center: new google.maps.LatLng(hasta.lat, hasta.lng),
			scrollwheel: false,
			zoom: 20,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		marker = new google.maps.Marker({
			position: hasta,
			map: map,
			icon: '/statics/1.1.2/images/pin_grey.png',
		});

		directionsDisplay.setMap(map);
		$("#route-instructions").empty();
		directionsDisplay.setPanel(document.getElementById('route-instructions'));

		if (exceptionHandler !== undefined) {
			setTimeout(function() {
				$("#foot").click();
			}, 200);
		}
	};

	centerMap = function() {
		if (map !== undefined) {
			google.maps.event.trigger(map, "resize");
		}
	};

	makeMarker = function(position, icon, title) {
		new google.maps.Marker({
			position: position,
			map: map,
			icon: icon
		});
	};

	buttonTap = function(type, directionsDisplay, directionsService) {
		centerStore = false;
		var mode, request;
		// Set destination, origin and travel mode.
		switch (type) {
			case "foot":
				$(".maps-btn i").removeClass("button-go-to-shop");
				$(".fa-male").addClass("button-go-to-shop");
				request = {
					destination: hasta,
					origin: desde,
					travelMode: google.maps.TravelMode.WALKING
				};
				break;
			case "drive":
				request = {
					destination: hasta,
					origin: desde,
					travelMode: google.maps.TravelMode.DRIVING
				};
				break;
			default:
				request = {
					destination: hasta,
					origin: desde,
					travelMode: google.maps.TravelMode.TRANSIT
				};
				break;
		}
		// Pass the directions request to the directions service.
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				// Display the route on the map.
				directionsDisplay.setDirections(response);
				var leg = response.routes[0].legs[0];
				makeMarker(leg.start_location, '/statics/1.1.2/images/pin_grey.png');
				makeMarker(leg.end_location, '/statics/1.1.2/images/pin_grey.png');
				setTimeout(function() {
					centerMap();
				}, 1000);
			}
		});
	};

	$('#mapaGoogle').on('shown.bs.modal', function() {
		if (map !== undefined) {
			google.maps.event.trigger(map, "resize");
		}
		if (centerStore) {
			var center = new google.maps.LatLng(hasta.lat, hasta.lng);
			map.setCenter(center);
			google.maps.event.trigger(map, "resize");
		}
	});

	locationUser();
	eventListeners();
	var access = getParameterByName('access');
	var showLog = getParameterByName('showLogin');
	if (access === "denied" || showLog === "true") {
		$('.modal-login-fragment').css('display', 'block');
		$("#modalLogin").modal('show');
	}
};

var clickScrollMark = function(elementHandle) {
	indiceScrollMark = 0;
	$(".scrollMark").each(function(index, key) {
		if (key.id === elementHandle.target.id) {
			indiceScrollMark = index;
		}
	});
};

var checkLocality = function() {
	if (city === 0) localitySelected = "Madrid";
	else localitySelected = "Barcelona";
};

var completeScrollMark = function() {
	setTimeout(function() {
		if ($(".scrollMark").eq(indiceScrollMark).parents(".main-store-list").length > 0) {
			$('html, body').animate({
				scrollTop: $(".scrollMark").eq(indiceScrollMark).parents(".main-store-list").offset().top - 10
			}, 1000, 'swing');
		}
		isotope();
	}, 100);
};


var personalizedPosition = function(idStore, distance, campaigns, followers, element) {
	var index_GTM = element.parents(".grid-item").index() + 1;
	var geo = distance > 0 ? true : false;
	if (geo) {
		push_bi_start('cd_distance', distance);
	} else {
		push_bi_start('cd_distance', 'no_geolocation');
	}
	push_bi_start('cd_shop_details', 'campaigns: ' + campaigns + ' _followers: ' + followers);
	push_bi_start('cd_personalized_position', index_GTM);
	push_bi('Home', 'SeeShopDetail_id_[' + idStore + ']', 'Home[-City][_search]', 'buttonClick');
};

var getAllStoresFromProvince = function() {
	checkLocality();
	if (typeof commandAllLocationsWithMap != 'undefined') {
		commandAllLocationsWithMap([{
			name: 'locality',
			value: localitySelected
		}]);
	}
};

var resizeAddress = function() {

	$('[data-toggle="tooltip"]').tooltip();
	var resizamos = false;
	$('.Store-Name.E4-5.home.direccion').each(function() {
		if (parseInt($(this).height()) >= 37) {
			resizamos = true;
		}
	});
	if (resizamos) {
		$('.Store-Name.E4-5.home.direccion').parent().css({
			'height': '70px',
			'text-align': 'center',
			'padding': '8% 0px'
		});
		$('.Store-Name.E4-5.home.direccion').css({
			'margin-left': '5px'
		});
	}

	$(".Name_store.home").each(function() {
		if ($(this).height() > 54) {
			$(this).children('.name-of-store').css('font-size', '22px');
		}
	});
	$(".name-of-store-app").each(function() {
		if ($(this).height() > 34) {

			$(this).css('text-align', 'left');
			$(this).css('bottom', '10px');
			$(this).css('left', '71px');
			$(this).css('line-height', '1');
		}
	});
	$(".noCupons").css("height", ($(".Buttons-categories").height() + 5) + "px");
};

var checkBalls = function() {
	$(".store-box").each(function(index, key) {
		var bolaAmount = 0;
		var bolaPercent = 0;
		var amountMax = 0;
		var percentMax = 0;
		$(this).find(".container-icon-voucher").each(function(indexAux, key) {
			if ($(this).data('typeaward') === "AMOUNT") {
				if (bolaAmount < parseInt($(this).data('award'))) {
					bolaAmount = parseInt($(this).data('award'));
					amountMax = indexAux;
				}

			} else {
				if (bolaPercent < parseInt($(this).data('award'))) {
					bolaPercent = parseInt($(this).data('award'));
					percentMax = indexAux;
				}
			}
		});
		if (bolaAmount > 0 || bolaPercent > 0) {
			if (bolaAmount >= 25) {
				$(this).find('.container-icon-voucher').eq(amountMax).prepend('<img src="/statics/1.1.2/images/' + bolaAmount + '€.png" class="icon-voucher" /> ');
				$(this).find('.container-icon-voucher').eq(amountMax).prevAll().remove();
				$(this).find('.container-icon-voucher').eq(0).siblings().remove();
			} else if (bolaAmount < 25 && bolaPercent > 0) {
				$(this).find('.container-icon-voucher').eq(percentMax).prepend('<img src="/statics/1.1.2/images/' + bolaPercent + '€.png" class="icon-voucher" /> ');
				$(this).find('.container-icon-voucher').eq(percentMax).prevAll().remove();
				$(this).find('.container-icon-voucher').eq(0).siblings().remove();
			} else if (bolaAmount < 25 && bolaPercent === 0) {
				$(this).find('.container-icon-voucher').eq(amountMax).prepend('<img src="/statics/1.1.2/images/' + bolaAmount + '€.png" class="icon-voucher" /> ');
				$(this).find('.container-icon-voucher').eq(amountMax).prevAll().remove();
				$(this).find('.container-icon-voucher').eq(0).siblings().remove();
			}
		}
	});
};

var checkStatePromo = function() {
	if (location.href.indexOf("my_stores") == -1) {
		var checkCard = $("#haveCard").val();
		$(".discount-box").each(function(index, el) {
			if (checkStateLogin()) {
				var couponType = $(this).find('.discount-value').data('typeoffer');
				var statusPromo = $(this).find('.discount-value').data('stateoffer');
				var haveMoreCoupons = $(this).find('.discount-value').data('havemore') === "have" ? true : false;
				$(this).css('display', 'block');
				if (couponType == 100 && statusPromo === "NOT_RESERVED") {
					$(this).parents(".Buttons-categories").find(".active-coupon").css('display', 'block');
				} else if (couponType == 100 && statusPromo === "PENDING") {
					$(this).parents(".Buttons-categories").find(".coupon-active").css('display', 'block');
				} else if (couponType == 5 && statusPromo === "NOT_RESERVED" && checkCard === "true") {
					if (haveMoreCoupons === false) {
						$(this).parents(".Buttons-categories").find(".active-coupon-of-store").css('display', 'block');	
					} else {
						$(this).parents(".Buttons-categories").find(".active-more-coupon-of-store").css('display', 'block');	
					}
				} else if (couponType == 5 && statusPromo === "NOT_RESERVED" && checkCard === "false") {
					$(this).parents(".Buttons-categories").find(".active-card-of-store").css('display', 'block');
				} else if (couponType == 5 && statusPromo === "PENDING") {
					$(this).parents(".Buttons-categories").find(".coupon-active-of-store").css('display', 'block');
				}
			} else {
				$(this).css('display', 'block');
				$(this).parents(".Buttons-categories").find(".not-loggued-active-coupon").css('display', 'block');
			}
		});
	}
	$(".discounts").each(function(index, el) {
		if ($(this).children('.discount-box').length === 0) {
			$(this).parents(".Buttons-categories").siblings('.noCupons').css('display', 'block');
			$(this).parents(".Buttons-categories").remove();
		}
	});
	if ($(".Buttons-categories").length > 0) {
		$(".noCupons").css("height", ($(".Buttons-categories").height() + 5) + "px");
	}
};

var checkPromotions = function() {
	$(".store-box").each(function(index, key) {
		var promotionUniversalIndex = null, promotionUniversalAmount = 0, promotionType = 0, promotionAmount = 0,
			promotionPercent = 0, amountMax = 0, percentMax = 0, promotionStatus = "";
			console.log($(this).find('.name-of-store-app').text());
		$(this).find(".discount-value").each(function(indexAux, key) {
			if ($(this).data('typeaward') === "AMOUNT") {
				if (promotionAmount < parseInt($(this).data('award'))) {
					promotionAmount = parseInt($(this).data('award'));
					promotionType = parseInt($(this).data('typeoffer'));

					amountMax = indexAux;
					if (promotionType === 100) {
						promotionUniversalAmount = promotionAmount;
						promotionUniversalIndex = indexAux;
					}
				}

			} else {
				if (promotionPercent < parseInt($(this).data('award'))) {
					promotionPercent = parseInt($(this).data('award'));
					promotionType = parseInt($(this).data('typeoffer'));

					if ($(this).data('stateoffer') === "PENDING" && promotionType != 100) {
						promotionStatus = "have";
					}
					percentMax = indexAux;
					if (promotionType === 100) {
						promotionUniversalIndex = indexAux;
						promotionUniversalAmount = promotionPercent;
					}
				}
			}
			console.log($(this).data('stateoffer'));
			console.log(promotionType);
			if ($(this).data('stateoffer') === "PENDING" && promotionType != 100) {
				promotionStatus = "have";
			}
		});
		if (promotionUniversalIndex !== null && promotionAmount <= promotionUniversalAmount && promotionPercent <= promotionUniversalAmount) {
			$(this).find('.discount-box').eq(promotionUniversalIndex).prepend('');
			$(this).find('.discount-box').eq(promotionUniversalIndex).prevAll().remove();
			$(this).find('.discount-box').eq(0).siblings().remove();
			$(this).find('.discount-box').find('.discount-value').data('havemore', promotionStatus);
		} else if (promotionAmount > 0 || promotionPercent > 0) {
			if (promotionAmount >= 25) {
				$(this).find('.discount-box').eq(amountMax).prepend('');
				$(this).find('.discount-box').eq(amountMax).prevAll().remove();
				$(this).find('.discount-box').eq(0).siblings().remove();
			} else if (promotionAmount < 25 && promotionPercent > 0) {
				$(this).find('.discount-box').eq(percentMax).prepend('');
				$(this).find('.discount-box').eq(percentMax).prevAll().remove();
				$(this).find('.discount-box').eq(0).siblings().remove();
			} else if (promotionAmount < 25 && promotionPercent === 0) {
				$(this).find('.discount-box').eq(amountMax).prepend('');
				$(this).find('.discount-box').eq(amountMax).prevAll().remove();
				$(this).find('.discount-box').eq(0).siblings().remove();
			} else if (promotionAmount < 25 && promotionPercent === 0) {
				$(this).find('.discount-box').eq(amountMax).prepend('');
				$(this).find('.discount-box').eq(amountMax).prevAll().remove();
				$(this).find('.discount-box').eq(0).siblings().remove();
			}
			$(this).find('.discount-box').find('.discount-value').attr('data-havemore', promotionStatus);
		}
	});
};

var checkMapaStatus = function() {
	if (mapaViewed === true) {
		$("#ver-mapa").css('display', 'none');
		$("#mostPopularStoresBox").css('display', 'none');
		$("#ver-listado").css('display', 'block');
		$("#map-stores").css('display', 'block');
		$("#container-button-view-more").css('display', 'none');
		$(".container-orderBy").css('display', 'none');
		mapaStoresSelected();
	} else {
		$("#ver-listado").css('display', 'none');
		$("#map-stores").css('display', 'none');
		$("#ver-mapa").css('display', 'block');
		$("#mostPopularStoresBox").css('display', 'block');
		$(".container-orderBy").css('display', 'block');
		checkStoresShow();
		isotope();
	}
};

var mapaStoresSelected = function() {
	var mapstores;
	var tiendaStores = [];
	var markers = [];

	var addListenersMarkerInfo = function() {
		google.maps.event.addListener(marker, 'click', function() {
			closeInfoWindow();
			this.infowindow.open(mapstores, this);
			var icon = this.getIcon();
			if (icon.indexOf('pin_green_select.png') != -1) {
				this.setIcon('/statics/1.1.2/images/pin_green.png');
			}

			if (icon.indexOf('pin_grey_select.png') != -1) {
				this.setIcon('/statics/1.1.2/images/pin_grey.png');
			}
		});

		google.maps.event.addListener(mapstores, 'click', function() {
			closeInfoWindow();
		});
	};

	var autoCenter = function() {
		//  Create a new viewpoint bound
		var bounds = new google.maps.LatLngBounds();
		//  Go through each...
		$.each(markers, function(index, marker) {
			bounds.extend(marker.position);
		});
		//  Fit these bounds to the map
		mapstores.fitBounds(bounds);
	};

	var closeInfoWindow = function() {
		var curMarker = this;
		// loop through all markers
		$.each(markers, function(index, marker) {
			// if marker is not the clicked marker, close the marker
			if (marker !== curMarker) {
				marker.infowindow.close();
				// marker.setIcon('/statics/1.1.2/images/pin_grey.png');
				var icon = marker.getIcon();
				if (icon.indexOf('pin_green') != -1) {
					marker.setIcon('/statics/1.1.2/images/pin_green_select.png');
				}

				if (icon.indexOf('pin_grey') != -1) {
					marker.setIcon('/statics/1.1.2/images/pin_grey_select.png');
				}

			}
		});
	};
	if (location.href.indexOf("my_stores") == -1) {
		$(".store-box").each(function(index, key) {
			tiendaStores.push(
				[
					parseFloat($(this).find(".latitudes").children().val()),
					parseFloat($(this).find(".latitudes").children().next("input").val()),
					$(this).find('.container-logo-store').children('img').attr('src'),
					$(this).find('.name-of-store-app').text(),
					$(this).find('.direction-of-store').text(),
					$(this).find(".latitudes").children().next("input").next("input").val(),
					$(this).find(".latitudes").children().next("input").next("input").next("input").val(),
					$(this).find(".latitudes").children().next("input").next("input").next("input").next("input").val(),
					$(this).find(".discount-value").data('typeaward') === "AMOUNT" ? '/statics/1.1.2/images/' + parseInt($(this).find(".discount-value").data('award')) + '€.png' : '/statics/1.1.2/images/' + parseInt($(this).find(".discount-value").data('award')) + '%.png',
					$(this).find('.discount-value').length > 0 ? '/statics/1.1.2/images/pin_green_select.png' : '/statics/1.1.2/images/pin_grey_select.png'
				]
			);
			console.log($(this).find(".discount-value").data('award'));
			if (isNaN(parseInt($(this).find(".discount-value").data('award')))) {
				tiendaStores[index][8] = undefined;
			}
		});
	} else {
		$(".store-box").each(function(index, key) {
			tiendaStores.push(
				[
					parseFloat($(this).find(".latitudes").children().val()),
					parseFloat($(this).find(".latitudes").children().next("input").val()),
					$(this).find('.container-logo-store').children('img').attr('src'),
					$(this).find('.name-of-store-app').text(),
					$(this).find('.direction-of-store').text(),
					$(this).find(".latitudes").children().next("input").next("input").val(),
					$(this).find(".latitudes").children().next("input").next("input").next("input").val(),
					$(this).find(".latitudes").children().next("input").next("input").next("input").next("input").val(),
					$(this).find(".container-icon-voucher").children().attr('src'),
					$(this).find('.container-icon-voucher').length > 0 ? '/statics/1.1.2/images/pin_green_select.png' : '/statics/1.1.2/images/pin_grey_select.png'
				]
			);
		});
	}
	if (typeof google != 'undefined') {
		mapstores = new google.maps.Map(document.getElementById('map-stores'), {
			center: new google.maps.LatLng(40.425214, -3.7123485),
			scrollwheel: true,
			zoom: 20,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var infowindow;
		var marker, i;

		for (i = 0; i < tiendaStores.length; i++) {
			var imagenVoucherMap = "<img src=''></img>";
			var tienda = {
				lat: tiendaStores[i][0],
				lng: tiendaStores[i][1],
				logo: tiendaStores[i][2],
				name: tiendaStores[i][3],
				address: tiendaStores[i][4],
				number: tiendaStores[i][5],
				hour: tiendaStores[i][6],
				urlMicrosite: tiendaStores[i][7],
				iconVoucher: tiendaStores[i][8],
				pin: tiendaStores[i][9]
			};
			if (tienda.iconVoucher !== undefined) {
				imagenVoucherMap = '<img src="' + tienda.iconVoucher + '"	class="icon-voucher icon-voucher-map" /> ';
			}
			infowindow = new InfoBox({
				content: "<div class='IFcontainer'>" +
					"<a href=" + tienda.urlMicrosite + ">" +
					imagenVoucherMap +
					"<div class='IFtitleBox' style='font-size: 1.2em;font-weight: 300; text-align:center'>" +
					"<img class='IFheader' src='" + tienda.logo + "'>" +
					"<span>" + tienda.name + "</span>" +
					"</div>" +
					"</a>" +
					"<div class='separator'></div>" +
					"<div class='direction-number-hour'>" +
					"<p class='store-address'>" + tienda.address + "</p>" +
					"<p>" + tienda.number + "</p>" +
					"<p class='store-hour'>Abierto de " + tienda.hour + "</p>" +
					"</div>" +
					"<div class='IFcorner'></div>" +
					"</div>",
				alignBottom: true,
				disableAutoPan: false,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(-188, -43),
				zIndex: null,
				pane: "floatPane",
				closeBoxURL: "",
				infoBoxClearance: new google.maps.Size(0, 60)
			});

			marker = new google.maps.Marker({
				position: new google.maps.LatLng(tienda.lat, tienda.lng),
				map: mapstores,
				infowindow: infowindow,
				icon: tienda.pin
			});

			markers.push(marker);
			addListenersMarkerInfo();
		}
		autoCenter();
	}
};