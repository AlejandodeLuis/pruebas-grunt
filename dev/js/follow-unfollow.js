$(document).on('click touchstart', '.markFav', function() {
	$(this).siblings(".markAsFavourite").click();
	if (checkFollowing()) {
		$(".modal-user-not-have-favourite-fragment").css("display","block");
		$("#modal-user-not-have-favourite").modal('show');
	}
	return false;
});

$(document).on('click touchstart', '.unmarkFav', function() {
	$(this).siblings(".unmarkAsFavourite").click();
	if (window.location.href.indexOf("my_stores") != -1) {
		$(this).parents(".main-store-list").remove();
	}
});

$(document).on("click touchstart", '.not-loggued-follow', function(event) {
	event.stopPropagation();
	if(!checkPromo()){
		$(".modal-login-reg-follow-fragment").css('display', 'block');
		$("#modalLog-Reg").modal('show');
	}else {
		$(".modal-login-reg-follow-promo-fragment").css('display', 'block');
		$("#modalLog-Reg-Promo").modal('show');	
	}
	return false;
});