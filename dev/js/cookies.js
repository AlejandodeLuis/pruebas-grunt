var allCookies = function(){
	$(document).on("click touchstart", '.element-nav-cookie', function() {
		/* Act on the event */
		$('#cookie-popup').fadeOut('slow');
		createCookie("AcceptCookie", "accept", "AcceptCookie");
	});

	$(document).on("click touchstart", '#cookie-popup', function(event) {
		if (event.target.className.indexOf("not-stop-propagation") === -1) {
			event.stopPropagation();
			$('#cookie-popup').show();
			return false;
		}
	});

	createCookie = function(name, value, who, days) {
		var dire = ""; //página a cargar en la popup
		var dias = days === undefined ? 30 : days; //días a los que caduca la cookie

		if ((document.cookie.indexOf("Promo=closedPromo") == -1 && who == "promo") || (document.cookie.indexOf("Splash=closedSplash") == -1 && who == "splash") || (document.cookie.indexOf("ApplicationMobile=closedApplicationMobile") == -1 && who == "mobile") || (document.cookie.indexOf("AcceptCookie=accept") == -1 && who == "AcceptCookie") || who === "possibles") {
			cad = new Date();
			cad.setTime(cad.getTime() + (dias * 24 * 60 * 60 * 1000));
			expira = "; expires=" + cad.toGMTString();
			path = "; path=/";
			document.cookie = name + "=" + value + expira + path;
		}
	};

	checkFollowing = function() {
		var following = getCookie("following");
		if (following.indexOf("[]") != -1) {
			return true;
		} else {
			return false;
		}
	};

	createCookie("Splash", "notSplash", "splash");
	createCookie("Promo", "notPromo", "promo", "3");
	createCookie("ApplicationMobile", "notClosedApplicationMobile", "mobile");
	createCookie("AcceptCookie", "notAccept", "AcceptCookie");

	if (document.cookie.indexOf("ApplicationMobile=closedApplicationMobile") == -1) {
		if (isMobile.iOS()) {
			$('.banner-ios').css('display', 'block');
		}
		if (isMobile.Android()) {
			$('.banner-android').css('display', 'block');
		}
	}

	if (document.cookie.indexOf("AcceptCookie=accept") == -1) {
		$('#cookie-popup').fadeIn('slow');
		createCookie("AcceptCookie", "notAccept", "AcceptCookie");
	}

	$('#modalValor').on('hidden.bs.modal', function(e) {
		e.stopPropagation();
		var cookie_name = 'Splash';
		var cookie_value = 'closedSplash';
		createCookie(cookie_name, cookie_value, "splash");
		$('#modalValor').css("display", "none");
		$('.modal-splash-fragment').css("display", "none");
		return false;
	});

	$('#modalValorPromo').on('hidden.bs.modal', function(e) {
		e.stopPropagation();
		var cookie_name = 'Promo';
		var cookie_value = 'closedPromo';
		createCookie(cookie_name, cookie_value, "promo");
		$('#modalValorPromo').css("display", "none");
		$('.modal-splash-promo-fragment').css("display", "none");
		return false;
	});

	$(document).on("click touchstart", "#go-to-register", function(e) {
		var cookie_name = 'Promo';
		var cookie_value = 'closedPromo';
		createCookie(cookie_name, cookie_value, "promo");
		window.location.href = "/shopping/register.xhtml";
	});

	$(document).on("click touchstart", '.closeX', function(event) {
		$('.banner-android').fadeOut("slow");
		$('.banner-ios').fadeOut("slow");
		var cookie_name = 'ApplicationMobile';
		var cookie_value = 'closedApplicationMobile';
		createCookie(cookie_name, cookie_value, "mobile");
		return false;
	});

	if (checkPromo() && !checkStateLogin()){
		if (document.cookie.indexOf("Promo=closedPromo") == -1) {
			$(".modal-splash-promo-fragment").css("display", "block");
			$('#modalValorPromo').modal('show');
		}
	}
};