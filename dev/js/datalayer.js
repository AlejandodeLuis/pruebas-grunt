var dataLayer = [];

function push_bi(category_, action_, label_, event_) {
	category_ = String(category_);
	action_ = String(action_);
	label_ = String(label_);
	event_ = String(event_);
	dataLayer.push({
		'category': category_,
		'action': action_,
		'label': label_,
		'event': 'eventga'
	});
}

function push_bi_start(key_, value_) {
	if (value_.target !== undefined) {
		value_ = $(this).index();
	}
	key_ = String(key_);
	value_ = String(value_);
	var key = key_;
	var obj = {};
	obj[key] = value_;
	dataLayer.push(obj);
}

function push_bi_end(key_, value_) {
	key_ = String(key_);
	value_ = String(value_);
	var key = key_;
	var obj = {};
	obj[key] = value_;
	dataLayer.push(obj);
}

function page_flow_error(action_, label_) {
	if ($("#panel-error").size() > 0) {
		var msg = $("#panel-error .errorlist").first().text();
		push_bi(action_ + '_KO_' + msg, label_);
		return 1; // KO
	}
	return 0; // OK
}