module.exports = function(grunt) { // Toda la configuración se encontará dentro de está funcion

  grunt.initConfig({ // Iniciamos la configuración de Grunt

    pkg: grunt.file.readJSON('package.json'), // Leemos el archivo packageJSON, lo que nos permitirá usar la información contenida en el archivo a lo largo de esta función
    copy: {
      common: {
        expand: true,
        dest: '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/',
        cwd: '<%= pkg.envs.dev %>',
        src: [
          'js/*', 'css/*', 'images/*'
        ]
      },
      inte: {
        expand: true,
        dest: '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/',
        cwd: '<%= pkg.envs.dev %>',
        src: [
          'js/infobox.js', 'js/gtm.js', 'css/*'
        ]
      },
      intestaHTML: {
        expand: true,
        dest: '<%= pkg.envs.int %>/',
        cwd: '<%= pkg.staticsHtmlDir %>',
        src: [
          '**/*'
        ]
      },
      prestaHTML: {
        expand: true,
        dest: '<%= pkg.envs.pre %>/',
        cwd: '<%= pkg.staticsHtmlDir %>',
        src: [
          '**/*'
        ]
      },
      prostaHTML: {
        expand: true,
        dest: '<%= pkg.envs.pro %>/',
        cwd: '<%= pkg.staticsHtmlDir %>',
        src: [
          '**/*'
        ]
      },
      pre: {
        expand: true,
        dest: '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/',
        cwd: '<%= pkg.envs.dev %>',
        src: [
          'js/infobox.js', 'js/gtm.js'
        ]
      },
      pro: {
        expand: true,
        dest: '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/',
        cwd: '<%= pkg.envs.dev %>',
        src: [
          'js/infobox.js', 'js/gtm.js'
        ]
      }
    },
    bowercopy: {
      options: {
        srcPrefix: 'bower_components'
      },
      scripts: {
        options: {
          destPrefix: '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>'
        },
        files: {

          //JS

          '/js/vendors/jquery-labelauty.js': 'jquery-labelauty/source/jquery-labelauty.js',
          '/js/vendors/jquery.min.js': 'jquery/dist/jquery.min.js',
          '/js/vendors/bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js',
          '/js/vendors/isotope.pkgd.min.js': 'isotope/dist/isotope.pkgd.min.js',
          '/js/vendors/imagesloaded.pkgd.min.js': 'imagesloaded/imagesloaded.pkgd.min.js',

          //CSS

          '/css/jquery-labelauty.min.css': '/jquery-labelauty/source/jquery-labelauty.css',
          '/css/bootstrap.min.css': '/bootstrap/dist/css/bootstrap.min.css'
        }
      }

    },
    concat: { // Iniciamos la configuración del plugin concat
      // options:{
      //  sourceMap: true
      // },
      dev: {
        files: {
          '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shoppingweb.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/cookies.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/follow-unfollow.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/login-register.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/coupons.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.js'], // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
          '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/bootstrap.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/imagesloaded.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/isotope.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery-labelauty.js'] // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
        },
      },
      inte: {
        files: {
          '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shoppingweb.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/cookies.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/follow-unfollow.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/login-register.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/coupons.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.js'], // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
          '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/bootstrap.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/imagesloaded.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/isotope.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery-labelauty.js'] // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
        },
      },
      pre: {
        files: {
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shoppingweb.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/cookies.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/follow-unfollow.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/login-register.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/coupons.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.js'], // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/bootstrap.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/imagesloaded.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/isotope.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery-labelauty.js'] // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
        },
      },
      pro: {
        files: {
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shoppingweb.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/cookies.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/follow-unfollow.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/login-register.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/coupons.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.js'], // Este es un patrón para seleccionar los archivos que deseamos concatenar y llevarlos a su destino
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/bootstrap.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/imagesloaded.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/isotope.pkgd.min.js', '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/jquery-labelauty.js']
        },
      }
    },
    uglify: {
      dev: {
        files: {
          '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.min.js': ['<%= pkg.envs.dev %>/js/datalayer.js']
        }
      },
      inte: {
        files: {
          '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.min.js': ['<%= pkg.envs.dev %>/js/datalayer.js']
        }
      },
      pre: {
        files: {
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js': ['<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/vendors.min.js'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.min.js': ['<%= pkg.envs.dev %>/js/datalayer.js'],
        }
      },
      pro: {
        files: {
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js': ['<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/shopping.min.js'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/datalayer.min.js': ['<%= pkg.envs.dev %>/js/datalayer.js']
        }
      }
    },
    cssmin: {
      inte: {
        files: {
          '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css'],
          '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css']
        }
      },
      pre: {
        files: {
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/Css_Limpio.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/Css_Limpio.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/infoBox.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/infoBox.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/404-503.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/404-503.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/reset.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/reset.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/dummy.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/dummy.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jsf_override.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jsf_override.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css'],
          '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css']
        }
      },
      pro: {
        files: {
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/Css_Limpio.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/Css_Limpio.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/infoBox.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/infoBox.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/404-503.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/404-503.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/reset.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/reset.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/dummy.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/dummy.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jsf_override.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jsf_override.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/bootstrap.min.css'],
          '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css': ['<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/css/jquery-labelauty.min.css']
        }

      }
    },
    imagemin: {
      inte: { // Task
        files: [{
          expand: true, // Enable dynamic expansion
          cwd: '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/', // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif,svg}'], // Actual patterns to match
          dest: '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/' // Destination path prefix
        }]
      },
      pre: { // Task
        files: [{
          expand: true, // Enable dynamic expansion
          cwd: '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/', // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif,svg}'], // Actual patterns to match
          dest: '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/' // Destination path prefix
        }]
      },
      pro: {
        files: [{
          expand: true, // Enable dynamic expansion
          cwd: '<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/', // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif,svg}'], // Actual patterns to match
          dest: '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/images/' // Destination path prefix
        }]
      }
    },
    jshint: {
      files: ['Gruntfile.js', '<%= pkg.envs.dev %>/js/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
    watch: {
      files: ['Gruntfile.js', 'dev/**/*'],
      tasks: ['copy:common', 'jshint', 'concat:dev']
    },
    // Deletes all .js files, but skips min.js files
    clean: {
      options: {
        'no-write': false,
        'force': true
      },
      all: ['<%= pkg.commonDir.dirResources %>/***/**/*', '<%= pkg.envs.pre %>/***/**/*', '<%= pkg.envs.pro %>/***/**/*', '<%= pkg.envs.int %>/***/**/*'],
      dev: ['<%= pkg.envs.dev %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.js', '<%= pkg.envs.dev %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.js', '!<%= pkg.envs.dev %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.min.js', '!<%= pkg.envs.dev %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.min.js'],
      inte: ['<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.js', '<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.js', '!<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.min.js', '!<%= pkg.envs.int %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.min.js'],
      pre: ['<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.js', '<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.js', '!<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.min.js', '!<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.min.js', '!<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/gtm.js', '!<%= pkg.envs.pre %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/infobox.js'],
      pro: ['<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.js', '<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.js', '!<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/vendors/*.min.js', '!<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/*.min.js', '!<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/gtm.js', '!<%= pkg.envs.pro %>/<%= pkg.commonDir.dirResources %>/<%= pkg.versionStatics %>/js/infobox.js']
    },
    compress: {
      inte: {
        options: {
          archive: '<%= pkg.building.int %>/ZION-STATIC-<%= pkg.versionStatics %>-SNAPSHOT.zip'
        },
        expand: true,
        cwd: '<%= pkg.envs.int %>/',
        src: ['**/*']

      },
      pre: {
        options: {
          archive: '<%= pkg.building.pre %>/ZION-STATIC-<%= pkg.versionStatics %>-SNAPSHOT.zip'
        },
        expand: true,
        cwd: '<%= pkg.envs.pre %>/',
        src: ['**/*']

      },
      pro: {
        options: {
          archive: '<%= pkg.building.pro %>/ZION-STATIC-<%= pkg.versionStatics %>.zip'
        },
        expand: true,
        cwd: '<%= pkg.envs.pro %>/',
        src: ['**/*']
      }
    },
    browserSync: {
      default_options: {
        bsFiles: {
          src: ['statics/<%= pkg.versionStatics %>/js/*.js', 'statics/<%= pkg.versionStatics %>/js/vendors/*.js', 'statics/<%= pkg.versionStatics %>/css/*.css']
        },
        options: {
          proxy: "localhost/shopping",
          watchTask: true
        }
      }
    },
    pageres: {
      iphone: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/iphone',
          delay: 10,
          userAgent: ["Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      android: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/android',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; LGMS323 Build/KOT49I.MS32310c) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.1599.103 Mobile Safari/537.36"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      windows: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/windows',
          delay: 10,
          userAgent: ["Mozilla/5.0 (MeeGo; NokiaN9) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      normal: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/normal',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      mac: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/mac',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      firefox: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/firefox',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      chrome: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/chrome',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      },
      explorer: {
        options: {
          urls: ['localhost/shopping/', 'localhost/shopping/stores-selected.xhtml'],
          sizes: ['1024x768', '1024x600', '1280x800', '1366x768', '1920x1080'],
          dest: 'Images_Responsive/explorer',
          delay: 10,
          userAgent: ["Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko"],
          filename: '{{date}} - {{url}}-{{size}}'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify'); // Aquí tenemos que añadir los módulos de npm que vayamos a usar
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-bowercopy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-pageres');


  grunt.registerTask('buildDevelopment', ['clean:all', 'copy:common', 'bowercopy', 'jshint', 'concat:dev', 'uglify:dev', 'browserSync', 'watch']);
  grunt.registerTask('buildIntegration', ['clean:all', 'copy:common', 'copy:intestaHTML', 'copy:inte', 'bowercopy', 'cssmin:inte', 'jshint', 'imagemin:inte', 'concat:inte', 'uglify:inte', 'compress:inte']); // Estas son las distintas task que queremos usar, la ruta por defecto se invoca escribiendo grunt a secas.
  grunt.registerTask('buildPreproduction', ['clean:all', 'copy:common', 'copy:prestaHTML', 'copy:pre', 'bowercopy', 'concat:pre', 'uglify:pre', 'cssmin:pre', 'imagemin:pre', 'clean:pre', 'compress:pre']);
  grunt.registerTask('buildProduction', ['clean:all', 'copy:common', 'copy:prostaHTML', 'copy:pro', 'bowercopy', 'concat:pro', 'uglify:pro', 'cssmin:pro', 'imagemin:pro', 'clean:pro', 'compress:pro']);
  grunt.registerTask('default', ['buildDevelopment']);
  grunt.registerTask('pajares', ['pageres:iphone', 'pageres:android', 'pageres:windows', 'pageres:normal', 'pageres:firefox', 'pageres:chrome', 'pageres:explorer', 'pageres:mac']);

};